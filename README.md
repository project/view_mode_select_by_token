# View Mode Select By Token

The goal of this module is to allow for the use of tokens in the view mode 
selection for a field.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/view_mode_select_by_token).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/view_mode_select_by_token).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable module
2. Create a field that you want to use as your selection and use the correct format for the 
   corresponding view mode (for example an image style of gallery_tall).
3. Go to the Manage Display tab.
4. There will now be an option of View Mode Select by Token in the Format selector for fields 
   in the Manage Display tab.
5. Put the token for that field you created into the View mode text field
6. Content editors can now dynamically control the view mode via the field on the node edit form.


## Maintainers

- Patrick Hangauer - [1mly](https://www.drupal.org/u/1mly)
- Aaron Zinck - [azinck](https://www.drupal.org/u/azinck)
